# PHPMyAdmin on OpenShift

## How to get this template in your cluster

Simply clone this repo and load the template into your own OpenShift project (or in the *openshift* project to make it usable to everyone in the cluster).

```sh
$ git clone https://gitlab.com/fcarrus/PHPMyAdmin-on-OpenShift.git
$ cd PHPMyAdmin-on-OpenShift

$ oc login -u myuser -p mypass 
$ oc create -f phpmyadmin-template.yaml

```

## What you need to use the template

If you already have a MariaDB/MySQL containerized db in your project, take note of the service name. Usually, it's just mariadb or mysql.

The Service name and the Secret name are usually the same, when you create your DB using the OpenShift base templates.

Retrieve your database Service name:
```sh
$ oc get svc 
```

You'll probably get something like:
```
mariadb       ClusterIP   172.30.242.165   <none>        3306/TCP            14m
```

Now process the PHPMyAdmin template using this info and choose a hostname for the Route that will be created, like:

```sh
$ oc process phpmyadmin-template MYSQL_SVCNAME=mariadb PHPMYADMIN_ROUTE=phpmyadmin.myopenshift.cluster | oc create -f -
```

Or use the Web GUI (Add to project -> Select from Project ) where you'll input that info in a comfortable input box ;-)

## What now?

Please wait while a BuildConfig is building the source code for PHPMyAdmin in a PHP:7.1 Source2Image container. 
The application starts as soon as the Image is built and pushed to the internal registry.

Then you can finally point your browser to https://the-host-you-chose. The config.inc.php is already set up for automatic login.

## The PHPMyAdmin configuration

The starting config.inc.php file is provided in the template, and uses references to the environment variables taken from the DB Secret, so you don't have to type them.
Of course this is very practical but extremely unsecure, since anyone could just point to the URL and mess with your data.

If you need to type in your username and password, then replace the contents of the phpmyadmin-config Secret with this:

```
<?php
/* Server configuration */

$cfg['Servers'][1]['host'] = getenv("MYSQL_SVCNAME");
$cfg['Servers'][1]['port'] = 3306;
$cfg['Servers'][1]['socket'] = '';
$cfg['Servers'][1]['auth_type'] = 'cookie';

/* End of servers configuration */

$cfg['DefaultLang'] = 'en';
$cfg['ServerDefault'] = 1;
$cfg['UploadDir'] = '';
$cfg['SaveDir'] = '';
?>
```

To do that, you can either do it through the Web GUI (Resources -> Config Maps -> phpmyadmin-config -> Actions -> Edit) or edit the complete YAML definition using the `oc` cli:

```sh
$ oc edit cm phpmyadmin-config
```

Re-deploy your PHPMyAdmin Deployment with the Web GUI (Applications -> Deployments -> phpmyadmin -> Deploy) or using the `oc` cli:
```sh
$ oc rollout latest phpmyadmin
```

Wait until the pod has restarted, and you're set. The PHPMyAdmin interface now asks for valid MariaDB/MySQL credentials, which you should have.

Happy Admin'ing!

